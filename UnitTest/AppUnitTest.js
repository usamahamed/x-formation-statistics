describe('X-Controller', function () {
    var scope, ctrl;
    beforeEach(module('myApp'));
 
    beforeEach(inject(function ($rootScope, $controller) {
          
        scope = $rootScope.$new();
        ctrl = $controller('X-Controller', {$scope: scope});
        scope.$digest();
        

    }));
 
    var contributorsFileName = "contributors.json";
    var ReposFileName = "https://api.github.com/users/x-formation/repos";

    it('must be TRUE and the data about contributions has been loaded', function () {
        expect(scope.getContributors(contributorsFileName)).toEqual(true);
    });

    it('must be TRUE and the data about repositories has been loaded', function () {
        expect(scope.getRepositories(ReposFileName)).toEqual(true);
    });
    
     it('Repositories chart arrays  has been loaded', function () {
        expect(scope.labelsRepochart).not.toBe(false);
        expect(scope.datarepochart).not.toBe(false);
        
    });
     it('Contributers chart arrays  has been loaded', function () {
        expect(scope.labelsconchart).not.toBe(false);
        expect(scope.dataconchart).not.toBe(false);
        
    });
     
    it('test routes is fine',
inject(function ($route) {

  expect($route.routes['/'].controller).toBe('X-Controller');
  expect($route.routes['/'].templateUrl).toEqual('Routers/home.html');

  expect($route.routes['/contributors'].controller).toBe('X-Controller');
  expect($route.routes['/contributors'].templateUrl).toEqual('Routers/contributors.html');

expect($route.routes['/contributorsTable'].controller).toBe('X-Controller');
  expect($route.routes['/contributorsTable'].templateUrl).toEqual('Routers/contributors_table.html');

expect($route.routes['/repositories'].controller).toBe('X-Controller');
  expect($route.routes['/repositories'].templateUrl).toEqual('Routers/repositories.html');

expect($route.routes['/repositoriesTable'].controller).toBe('X-Controller');
  expect($route.routes['/repositoriesTable'].templateUrl).toEqual('Routers/repositories_table.html');


}));

    
    
});
    
  