   /*
 * X-Formation statistics
 * Version:1.0
 * Author: Usama Hamed
 * Angular modules 
 */
   (function (ChartJsProvider) {
  ChartJsProvider.setOptions({ colors :[ [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']] });
}); 
(function () {

    var app = angular.module("myApp", ["ngRoute","chart.js"]);


    app.controller('X-Controller', ['$scope', '$http', function ($scope, $http) {
            $scope.labelsCon=[];
		$scope.dataCon=[];
 $scope.labelsRepo=[];
		$scope.dataRepo=[];

					$scope.sortColumnCon = "contributions";
                                        $scope.sortColumnRepo = "forks_count";
                                        
					$scope.reverseSort = true;

					$scope.sortDataCon = function(column)  {
						$scope.reverseSort = ($scope.sortColumnCon === column) ? !$scope.reverseSort : false;
						$scope.sortColumnCon = column;
                                                $scope.data11.sort();
                                                
					};
                                        
					$scope.getSortClassCon = function (column) {
						if ($scope.sortColumnCon === column) {
							return $scope.reverseSort ? 'arrow-down' : 'arrow-up';
						}
						return '';
					};
                                        $scope.sortDataRepo = function(column)  {
						$scope.reverseSort = ($scope.sortColumnRepo === column) ? !$scope.reverseSort : false;
						$scope.sortColumnRepo = column;
					};
                                        
					$scope.getSortClassRepo = function (column) {
						if ($scope.sortColumnRepo === column) {
							return $scope.reverseSort ? 'arrow-down' : 'arrow-up';
						}
						return '';
					};
            
					
        $scope.getContributors = function (fileName) {
            $http.get(fileName).success(function (data) {
                $scope.users = data;
                          angular.forEach(data, function(item){
                $scope.labelsCon.push(item.nickname);
                 $scope.dataCon.push(item.contributions);
                  console.log(item.nickname);
      
   });
            });
            //chart draw
$scope.labelsconchart = $scope.labelsCon;
  $scope.series = ['Series A', 'Series B'];
  $scope.dataconchart = $scope.dataCon;
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
//  $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
  $scope.options = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ]
    }
  };
//end char drawing
            return true;
        };

        $scope.getRepositories = function (path) {
            $http.get(path)
                .success(function (data) {
                    var json = JSON.stringify(data);
                    $scope.repositiories = data;
              angular.forEach(data, function(item){
                $scope.labelsRepo.push(item.name);
                 $scope.dataRepo.push(item.forks_count);
      
   });
                    console.log(json);
                    
                })
                .error(function ( status) {
                    console.log(status);
                    console.log("Error occured");
                });
                
                            //chart draw
$scope.labelsRepochart = $scope.labelsRepo;
  $scope.series1 = ['Series A', 'Series B'];
  $scope.datarepochart = $scope.dataRepo;
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  //$scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
  $scope.options1 = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ]
    }
  };
//end char drawing
            return true;
        };
    }]); //TestController

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.withCredentials = false;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
    }]); // httpConfig

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'Routers/home.html',
                    controller: 'X-Controller'
                }, null)
                 .when('/contributors', {
                    templateUrl: 'Routers/contributors.html',
                    controller: 'X-Controller'
                }, null)
                .when('/contributorsTable', {
                    templateUrl: 'Routers/contributors_table.html',
                    controller: 'X-Controller'
                }, null)
                .when('/repositories', {
                    templateUrl: 'Routers/repositories.html',
                    controller: 'X-Controller'
                }, null)
                .when('/repositoriesTable', {
                    templateUrl: 'Routers/repositories_table.html',
                    controller: 'X-Controller'
                });
        }]); // RouteConfig

})();
//home page chart graph
var c1 = document.getElementById("c1");
var parent = document.getElementById("p1");
c1.width = parent.offsetWidth;
c1.height = parent.offsetHeight;
var data1 = {
  labels : ["00","10","20","30","40","50","60"],
  datasets : [
    {
      fillColor : "rgba(255,255,255,.1)",
      strokeColor : "rgba(255,255,255,1)",
      pointColor : "#0af",
      pointStrokeColor : "rgba(255,255,255,1)",
      data : [150,200,235,290,300,350, 450]
    }
  ]
};

var options1 = {
  scaleFontColor : "rgba(255,255,255,1)",
  scaleLineColor : "rgba(255,255,255,1)",
  scaleGridLineColor : "transparent",
  bezierCurve : false,
  scaleOverride : true,
  scaleSteps : 5,
  scaleStepWidth : 100,
  scaleStartValue : 0
};

new Chart(c1.getContext("2d")).Line(data1,options1);